﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour {
	TileLoader tileLoader;
	public int x;
	public int y;
	public bool activated = false;
	public Color startColor;
	Vector3 startPosition;
	
	void Awake()
	{
		tileLoader = GameObject.Find("Game").GetComponent<TileLoader>();
		startColor = renderer.material.color;
		startPosition = transform.position;
	}
	
	void Update()
	{
		if (activated)
			renderer.material.color = Color.cyan;
		else
			renderer.material.color = startColor;
		
		if (Vector3.Distance(startPosition, transform.position) > 0.01f)
		{
			Vector3 direction = startPosition - transform.position;
			transform.Translate(direction * 2.0f * Time.deltaTime);
			print(startPosition);
		}
	}

	void OnMouseOver () 
	{
		if (Input.GetMouseButtonDown(0) && !activated)
		{
			if (!tileLoader.swapping && renderer.enabled)
			{
				if (!tileLoader.tileActive)
				{
					activated = true;
					tileLoader.tileActivated(x, y);
				}
				else
				{
					if (!tileLoader.checkAdjacentTile(x, y))
					{
						tileLoader.deactivateTile();
						activated = true;
						tileLoader.tileActivated(x, y);
					}
					else
					{
						tileLoader.swapTile(x, y);
						tileLoader.deactivateTile();
					}
				}
			}
		}
	}
}
