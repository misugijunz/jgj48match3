using System;
using UnityEngine;
using System.Collections;


public class GameState : MonoBehaviour
{	
	public float timer;
	public int score;
	public int coin;
	public int chain;
	public float chainTimer;
	public bool isChaining;
	public int scoreMultiplier;
	
	private GameStateType gameState;
	
	private enum GameStateType 
	{
    	PLAY = 0,
    	PAUSE = 1,
    	GAMEOVER = 2
	}
	
	public GameState ()
	{
		timer = 60.0f;
		score = 0;
		coin = 0;
		chain = 1;
		chainTimer = 5.0f;
		isChaining = false;
	}
	
	public void Awake()
	{
		timer = 60.0f;
		score = 0;
		coin = 0;
		chain = 1;
		chainTimer = 5.0f;
		isChaining = false;
		gameState = GameStateType.PLAY;
	}
	
	public void Update()
	{
		this.timer -= Time.deltaTime;
		
		//test if chaining reaction
		if (isChaining)
		{
			this.chainTimer -= Time.deltaTime;	
		}
		
		if (this.timer == 0.0f)
		{
			gameState = GameStateType.GAMEOVER;	
		}
		
		if (this.chainTimer == 0.0f)
		{
			isChaining = false;
			resetChainTimer();
		}
	}
	
	public void resetChain()
	{
		this.chain = 1;	
	}
	
	public void addChain()
	{
		this.chain++;	
	}
	
	public void addCoin(int coin)
	{
		this.coin += coin;	
	}
	
	public void addScore(int score)
	{
		this.score += score;	
	}
	
	public void resetChainTimer()
	{
		this.chainTimer = 5.0f;	
	}
	
	public void chaining()
	{
		isChaining = true;	
	}
	
	///TO DO: Create Timer buat puzzlenya
	
	
	///TO DO: Create Timer buat chain reaction, kalau ada chain, timer reset ke 5
}


