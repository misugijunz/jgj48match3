﻿using UnityEngine;
using System.Collections;

public class TileLoader : MonoBehaviour {
	[SerializeField]
	GameObject[] tileModel;
	
	Vector3 tilePos;
	int[][] level;
	
	public bool tileActive = false;
	int tileActiveX;
	int tileActiveY;
	GameObject currentTiles;
	GameObject swapTiles;
	public bool swapping = false;
	float swapTimer = 0.0f;
	
	void Start () 
	{
		level = new int[8][];
		
		tilePos = transform.position;
		
		for (int i = 0; i < 8; i++)
		{
			level[i] = new int[8];
			for (int j = 0; j < 8; j++)
			{
				tilePos.x = j;
				tilePos.y = i;
				
				do
				{
					level[i][j] = Random.Range(1, tileModel.Length + 1);
				}
				while (isStreak(i, j));

				GameObject tileClone = Instantiate(tileModel[level[i][j] - 1], tilePos, transform.rotation) as GameObject;
				tileClone.GetComponent<Tile>().x = j;
				tileClone.GetComponent<Tile>().y = i;
			}
		}
	}
	
	void Update()
	{

	}
	
	bool checkGem(int gem, int row, int col)
	{
		if (level[row] == null) {
			return false;
		}
		if (level[row][col] == 0) {
			return false;
		}
		
		return gem == level[row][col];
	}
	
	int rowStreak (int row, int col)
	{
		int current = level[row][col];
		int streak = 1;
		int tmp = col;
		
		while (tmp > 0 && checkGem(current, row, tmp - 1))
		{
			tmp--;
			streak++;
		}
		
		tmp = col;
		
		while (tmp < 7 && checkGem(current, row, tmp + 1)) 
		{
			tmp++;
			streak++;
		}
		
		return (streak);
	}
	
	int colStreak (int row, int col)
	{
		int current = level[row][col];
		int streak = 1;
		int tmp = row;
		
		while (tmp > 0 && checkGem(current, tmp - 1, col))
		{
			tmp--;
			streak++;
		}
		
		tmp = row;
	
		while (tmp < 7 && checkGem(current, tmp + 1, col)) 
		{
			tmp++;
			streak++;
		}
			
		return (streak);
	}
	
	bool isStreak(int row, int col)
	{
		return rowStreak(row,col)>2 || colStreak(row,col)>2;
	}
	
	public void tileActivated(int x, int y)
	{
		tileActive = true;
		tileActiveX = x;
		tileActiveY = y;
	}
	
	public void deactivateTile()
	{
		tileActive = false;
		GameObject[] tileClone = GameObject.FindGameObjectsWithTag("Tile");
		foreach (GameObject tiles in tileClone)
		{
			tiles.GetComponent<Tile>().activated = false;	
		}
		tileActiveX = -1;
		tileActiveY = -1;
	}
	
	public bool checkAdjacentTile(int x, int y)
	{
		if ((y == tileActiveY + 1 || y == tileActiveY - 1) && x == tileActiveX) {
			return true;
		}
		return (x == tileActiveX + 1 || x == tileActiveX - 1) && y == tileActiveY;	
	}
	
	public void swapTile(int x, int y)
	{
		swapArray(y, x, tileActiveY, tileActiveX);
		if (isStreak(y, x) || isStreak(tileActiveY, tileActiveX))
		{
			GameObject[] tileClone = GameObject.FindGameObjectsWithTag("Tile");
			foreach (GameObject tiles in tileClone)
			{
				Tile tile = tiles.GetComponent<Tile>();
				if (tile.x == x && tile.y == y) currentTiles = tiles;
				if (tile.x == tileActiveX && tile.y == tileActiveY) swapTiles = tiles;
			}
			
			Color tmpColor = currentTiles.GetComponent<Tile>().startColor;
			currentTiles.renderer.material.color = swapTiles.GetComponent<Tile>().startColor;
			currentTiles.GetComponent<Tile>().startColor = currentTiles.renderer.material.color;
			swapTiles.renderer.material.color = tmpColor;
			swapTiles.GetComponent<Tile>().startColor = tmpColor;
//			
//			Vector3 tmpPos = currentTiles.transform.position;
//			currentTiles.transform.position = swapTiles.transform.position;
//			swapTiles.transform.position = tmpPos;
			
			if (isStreak(y, x))
			{
				removeTile(y, x);
			}
			if (isStreak(tileActiveY, tileActiveX))
			{
				removeTile(tileActiveX, tileActiveY);	
			}
			
		}
		else 
		{
			swapArray(y, x, tileActiveY, tileActiveX);
		}		
	}
	
	void swapArray(int x, int y, int x2, int y2)
	{
		int tmp = level[x][y];
		level[x][y] = level[x2][y2];
		level[x2][y2] = tmp;
	}
	
	void removeTile(int row, int col)
	{
		int[] gemsToRemoveX = new int[8*8];
		int[] gemsToRemoveY = new int[8*8];
		
		for (int i = 0; i < 8*8; i++)
		{
			gemsToRemoveX[i] = -1;
			gemsToRemoveY[i] = -1;
		}
		
		int index = 0;
		int current =level[row][col];
		int tmp;
		
		gemsToRemoveY[index] = row;
		gemsToRemoveX[index] = col;
		index++;
		
		if (rowStreak(row,col)>2) {
			tmp = col;
			
			while (tmp > 0 && checkGem(current,row,tmp-1)) {
				tmp--;
				gemsToRemoveY[index] = row;
				gemsToRemoveX[index] = tmp;
				index++;
			}
			
			tmp=col;
			
			while (tmp < 7 && checkGem(current,row,tmp+1)) {
				tmp++;
				gemsToRemoveY[index] = row;
				gemsToRemoveX[index] = tmp;
				index++;
			}
		}
		
		if (colStreak(row,col)>2) {
			tmp=row;
			
			while (tmp > 0 && checkGem(current,tmp-1,col)) {
				tmp--;
				gemsToRemoveY[index] = tmp;
				gemsToRemoveX[index] = col;
				index++;
			}
			
			tmp=row;
			
			while (tmp < 7 && checkGem(current,tmp+1,col)) {
				tmp++;
				gemsToRemoveY[index] = tmp;
				gemsToRemoveX[index] = col;
				index++;
			}
		}
		
		GameObject[] tileClone = GameObject.FindGameObjectsWithTag("Tile");
		foreach (int gemX in gemsToRemoveX)
		{
			foreach (int gemY in gemsToRemoveY)
			{
				foreach (GameObject tiles in tileClone)
				{
					Tile tile = tiles.GetComponent<Tile>();
					if (tile.x == gemX && tile.y == gemY)
					{
						level[gemY][gemX] = -1;
						Destroy(tiles);
					}
				}	
			}
		}
		
		adjustTile();
	}
	
	void adjustTile()
	{
		for (int i=0; i<8; i++) 
		{
			for (int j=0; j<8; j++) 
			{
				if (level[i][j] == -1)
				{
					tilePos.x = j;
					tilePos.y = i;
					level[i][j] = Random.Range(1, tileModel.Length + 1);
					GameObject tileClone = Instantiate(tileModel[level[i][j] - 1], tilePos, transform.rotation) as GameObject;
					tileClone.GetComponent<Tile>().x = j;
					tileClone.GetComponent<Tile>().y = i;
					
					if (isStreak(j, i))
					{
						removeTile(j, i);
					}
				}
			}
		}
	}
}
